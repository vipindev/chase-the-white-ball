#include "ros/ros.h"
#include "ball_chaser/DriveToTarget.h"
#include <sensor_msgs/Image.h>

// Define a global client that can request services
ros::ServiceClient client;

// This function calls the command_robot service to drive the robot in the specified direction
void drive_robot(float lin_x, float ang_z)
{
    // TODO: Request a service and pass the velocities to it to drive the robot
    ball_chaser::DriveToTarget srv;
    srv.request.linear_x = lin_x;
    srv.request.angular_z = ang_z;
    ROS_INFO("LINEAR X : %f, ANGULAR Z : %f", lin_x,ang_z);
    if(!client.call(srv)){
        ROS_ERROR("Failed to call service drive to target.");
    }
}

// This callback function continuously executes and reads the image data
void process_image_callback(const sensor_msgs::Image img)
{

    int white_pixel = 255;
    bool found_a_white_pixel = false;
    int position = -1;
    for (int i = 0; i < img.height * img.step; i++) {
        if (img.data[i] == white_pixel) {
            found_a_white_pixel = true;
            if(i% img.step <= img.step / 3){
               
                position = 1;
               
            }else if(i% img.step >= img.step / 3 && i% img.step <= 2 * (img.step / 3)){
             
                 position  = 2;
               
            }else 
                position = 3;
            }
        }
    if(!found_a_white_pixel){
        ROS_INFO("Couldn't find a white pixel...Stopping.");
        drive_robot(0.0,0.0);
    } else {
        switch(position) {
            case -1 : drive_robot(0.0,0.0);break;
            case 1  : drive_robot(2,2);;break;
            case 2 :  drive_robot(2,0.0); break;
            case 3 :  drive_robot(2,-2);break;
        }
    }
}

int main(int argc, char** argv)
{
    // Initialize the process_image node and create a handle to it
    ros::init(argc, argv, "process_image");
    ros::NodeHandle n;

    // Define a client service capable of requesting services from command_robot
    client = n.serviceClient<ball_chaser::DriveToTarget>("/ball_chaser/command_robot");

    // Subscribe to /camera/rgb/image_raw topic to read the image data inside the process_image_callback function
    ros::Subscriber sub1 = n.subscribe("/camera/rgb/image_raw", 1, process_image_callback);

    // Handle ROS communication events
    ros::spin();

    return 0;
}